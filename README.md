# Cursos
- [AWS Cloud Practitioner Essentials](https://pt.coursera.org/learn/aws-cloud-practitioner-essentials)


# CLOUD
A aws define a cloud com a seguinte frase: `The on-demand delivery of IT resources over the internet with pay-as-you-go pricing`. Quebrando essa frase, podemos dividir ela em quatro partes:

- `The on-demand`:
    - A cloud tem os recussos que você precisa, quando você precisa. Ou seja, se você precisar de 5 EC2 agora, é só instânciar elas, e se não precisar mais, é só terminar as instânicas, deixando de pagar imediatamente

- `IT resources`:
    - É o fornecimento de diversos recursos e serviços que são comuns à todos os tipos de aplicações, como por exemplo a engine de um banco de dados, serviços de fila e etc... Todos esses serviços não são diferenciais em seu negócio, eles são apenas componentes de TI que são bem comuns, a aws utiliza o termo `Undifferentiated heavy lifting of IT`. Ou seja, tarefas e serviços que são comuns, mas que são difíceis e caros de manter.

- `Over the internet`:
    - Implica que você pode acessar os recursos pela internet, seja pelo console da aws, ou por meio de CLI ou SDK.

- `Pay-as-you-go pricing`:
    - Pague somente o que usar, pelo tempo que estiver utilizando.

## Deploy
Temos três formas de deploy de aplicações:

- Cloud-Based:
    - Totalmente em um ambiente de cloud, como a aws, em que você utiliza os recursos da cloud para melhorar sua aplicação, e para facilitar a mantenibilidade dela, assim como o custo variável.

- On-Premises
    - Envolve a compra de todos os servidores, bem como o local para manté-los, energia, e pessoal para instalar e manté-los.

- Hybrid:
    - Mantém parte da infra na cloud e parte on-premises. Normalmente é um modelo viável para aplicações legado, que estão migrando para a cloud.

## Benefícios da cloud
A cloud apresenta diversos benefícios, sendo eles:

- Trocar dispesas iniciais por despesas variáveis:
    - Caso você escolha começar sua aplicação em um ambiente on-premises, você terá que gastar muito dinheiro com espaço, e servidores. Com a cloud, você não tem esse gasto inicial muito grande, ele vai variar somente de acordo com o que realmente for necessário, reduzindo muito o gasto inicial.

- Pare de gastar dinheiro para construir e manter data centers:
    - Com a cloud, não é necessário comprar nenhum servidor, muito menos manter e instalar esses servidores. Logo não é necessário gastar com esses recursos físicos, que são muito caros.

- Pare de adivinhar capacidade:
    - Com a cloud, você não precisa adivinhar/estimar quanto de capacidade computacional você irá precisar. Vamos supor que você comporou servidores para suportar 10000 usuários diários, mas não atingiu nem 1000 usuários. Todo o gasto extra em máquinas foi desperdissado, e como você já comprou as máquinas, você está preso com esse gasto desnecessário. E também pode acontecer o contrário, você supõe 1000 usuários diários, mas está recebendo 10000, logo você irá enfrentar indisponibilidade, e o tempo até chegarem novas máquinas, instalá-las e configurá-las pode levar dias, semanas, ou meses.O que pode significar a perda de usuários. Com a cloud, você pode simplesmente redimencionar sua máquina de acordo com a sua necessidade momentânea. Assim você não irá gastar de forma desncessária, e nem sofrerá com indisponibilidade.

- Aumento da velocidade e agilidade:
    - Como novos recursos estão a literalmente poucos cliques de distância, é muito fácil e rápido de criar novas aplicações ou testar ideias. Assim como é bem simples e rápido desmontar todos os recursos necessários, caso não tenha obtido sucesso nos testes. No caso de uma abordagem on-premises, é muito complicado efetuar experimentos, pois a compra de novos servidores leva tempo, e caso não deem certo, não tem como devolver os servidores e pegar o dinheiro de volta.

- Global em minutos:
    - Como o provedor de cloud possui uma infra global, como é o caso da aws, a aplicação pode escalar a nível global automaticamente, com alta disponibilidade e baixa latência. Uma vez que é bem simples alocar recursos em multiplas regiões no mundo.

# Regions
A aws é dividida em diversas regiões do mundo. Isso já nos ajuda de duas formas, a primeira é com a possibilidade de rodar nossas aplicações o mais proximo possível, com menor latência, e também nos prepararmos para eventuais desastres naturais.

Em um caso de estrutura on-premises, você pode se preparar com data centers em mais de um lugar do mundo, e caso algo de errado no principal você liga o secundário, mas o problema é que você tem que comprar todo esse hardware e esperar por um problema que pode nunca acontecer.

Com a aws esse problema não existe, pois fica fácil mover a infra caso necessário.

Porém tenha em mente que uma região é completamente issolada da outra. Não tem como os dados trafegar entre elas a menos que você explicitamente mova eles. Logo você precisa escolher uma região para trabalhar. Seus critérios para escolher uma região são: 
- Leis: 
    - Os dados gerados no país, podem ficar apenas no país
- Proximidade:
    - Latência pode ser um problema. Então escolher uma região mais proxima vale a pena
- Recursos:
    - Nem todas as regiões possuem todos os recursos. As vezes a aws ainda não instalou o hardware suficiente naquela região para implementar um recurso X que você precisa.
- Custo:
    - No Brasil é tão mais caro que compensa usar uma região nos EUA

Nesse link [aqui](https://aws.amazon.com/pt/about-aws/global-infrastructure/) você pode ver todas as regiões da aws no mundo.

Essas regiões são denominadas como `us-east-1`, `us-east-2`, `us-west-1`...

## Availability Zones

Bom, se você notou que o você precisa de estar em uma unica região, você deve ter notado que o problema de desastres naturais não foi resolvido a menos que você dobre sua infra.

Bom, para isso que uma região é compasta de zonas de disponibilidades (AZ). Uma região é composta de no minimo três AZs, sendo que cada AZ é um data-center, e eles possuem conexão entre si, com uma latência ridiculamente baixa, e esses centros ficam a alguns quilômetros de distância um do outro, garantindo que um desastre natual não afete todas as AZs de uma vez só.

Quando você estiver criando um recurso na aws, sempre tenha em mente de colocar esse recurso em **ao menos duas** zonas de disponibilidade. Sendo que alguns recursos são nativamente em nível de região (`regionaly scoped service`). Ou seja, ele já existe em todas as AZs dessa região, e você não precisa fazer nada quanto a isso.

Um exemplo de recurso que você deve manejar em mais de uma região, são instâncias EC2 (proximo tópico), e recursos no nível de região é o S3 (também falo dele mais para frente).

Nesse link [aqui](https://aws.amazon.com/pt/about-aws/global-infrastructure/regions_az/) você pode ver todas as regiões e suas AZs pelo mundo.

Essas AZs são denominadas como: `us-east-1a`, `us-east-1b`, `us-east-1c` ...
